<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\Auths\Authenticatable;
use App\Models\Auths\Customer;
use App\Models\Auths\User;
use Exception;
use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;
use Response;

class AuthController extends AccessTokenController
{
    public function register(Request $request)
    {
        $attributes = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:sys_users'],
            'phone' => ['required', 'string', 'max:20', 'unique:sys_users'],
            'job'   => ['nullable'],
            'title' => ['nullable'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        DB::beginTransaction();

        try {
            $user = User::create([
                'name'  => $attributes['name'],
                'email' => $attributes['email'],
                'phone' => $attributes['phone'],
                'job'   => $attributes['job'],
                'title' => $attributes['title'],
                'password' => Hash::make($attributes['password']),
            ]);

            $client = \Laravel\Passport\Client::where('password_client', 1)->first();

            $request->request->add([
                'grant_type'    => 'password',
                'client_id'     => $client->id,
                'client_secret' => $client->secret,
                'username'      => $attributes['email'],
                'password'      => $attributes['password'],
                'scope'         => null,
            ]);

            // Fire off the internal request.
            $proxy = Request::create(
                'oauth/token',
                'POST'
            );

            $resp = \Route::dispatch($proxy);
        } catch (Exception $e) {
            ////return error message
            DB::rollback();
            return response(["message" => "Internal server error"], 500);
        }

        DB::commit();

        return response()->json([
            'user' =>  new UserResource($user),
            'token' => json_decode($resp->getContent())->access_token
        ]);
    }

    public function login(ServerRequestInterface $request)
    {
        try {
            //get username (default is :email)
            $username = $request->getParsedBody()['username'];

            //get user
            //change to 'email' if you want
            $user = User::where('email', '=', $username)->first();

            //generate token
            $tokenResponse = parent::issueToken($request);

            //convert response to json string
            $content = $tokenResponse->getContent();

            //convert json to array
            $data = json_decode($content, true);

            if (isset($data["error"])) {
                throw new OAuthServerException('The user credentials were incorrect.', 6, 'invalid_credentials', 401);
            }

            return response()->json([
                'user' =>  new UserResource($user),
                'token' => $data['access_token']
            ]);
        } catch (ModelNotFoundException $e) { // email notfound
            //return error message
            return response(["message" => "User not found"], 422);
        } catch (OAuthServerException $e) { //password not correct..token not granted
            //return error message
            return response(["message" => 'The user credentials were incorrect.'], 422);
        } catch (Exception $e) {
            ////return error message
            return response(["message" => "Internal server error"], 500);
        }
    }

    public function profile()
    {
        return new UserResource(request()->user());
    }

    public function update()
    {
        $user = request()->user();

        $attributes = request()->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:sys_users,email,'.request()->user()->id],
            'phone' => ['required', 'string', 'max:20', 'unique:sys_users,phone,'.request()->user()->id],
            'job'   => ['nullable'],
            'title' => ['nullable'],
            'photo' => ['nullable'],
            'password' => ['nullable', 'string', 'min:6', 'confirmed'],
        ]);

        if($photo = request()->file('photo')){
            if(!is_null($user->photo)){
                Storage::delete($user->photo);
            }
            $attributes['photo'] = $photo->store('users/'.request()->user()->id, 'public');
        }else{
            unset($attributes['photo']);
        }

        if(is_null($attributes['password'])){
            unset($attributes['password']);
        }

        $user->update($attributes);

        return new UserResource($user);
    }
}

<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\CoreSn;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;
use App\Models\Task\Task;
use App\Models\Auths\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $routes = 'dashboard.home';
    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	$user = Task::where('status', 1)->get();
    	$arraypt = array();
    	$datas ='';
    	if($user){
	    	foreach ($user as $key => $value) {
		    	$arraypt[] = [
				                'startDate' => \Carbon\Carbon::parse($value->tanggal)->format('m/d/Y'),
				                'endDate' => \Carbon\Carbon::parse($value->tanggal)->format('m/d/Y'),
				                'summary' => $value->employe->name,
				            ];
	    	}
    	}
        return $this->render('modules.dashboard.home', [
        	'mockup' => true,
        	'user' => $user,
        	'jadwal' => $arraypt,
        ]);
    }

    public function chartTask(Request $request)
    {
    	$aktif = array();
    	$naktif = array();
    	for($i=0;$i<12;$i++){
	    		$bulans = sprintf("%02d", ($i+1) );
				$cari = Task::whereMonth('tgl', $bulans)->whereYear('tgl', $request->tahun)->where('status', 1)->get()->count();
				$aktif[$bulans] = $cari;
	    }

	    for($i=0;$i<12;$i++){
	    		$bulans = sprintf("%02d", ($i+1) );
				$cari = Task::whereMonth('tgl', $bulans)->whereYear('tgl', $request->tahun)->where('status', 0)->get()->count();
				$naktif[$bulans] = $cari;
	    }
    	return $this->render('modules.dashboard.grafik', [
            'aktif' => implode(",", $aktif),
            'naktif' => implode(",", $naktif),
        ]);
    }

    public function chartEmploye(Request $request)
    {
    	$aktif = User::whereHas('roles', function($u){
    					$u->where('name', 'employe');
    				  })->where('status', 1)->get()->count();
		$naktif = User::whereHas('roles', function($u){
    					$u->where('name', 'employe');
    				  })->where('status', 0)->get()->count();
		$data=[$aktif, $naktif];
    	return $this->render('modules.dashboard.employe', [
            'data' => implode(",", $data),
        ]);
    }
}

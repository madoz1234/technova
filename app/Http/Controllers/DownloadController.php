<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Files;
use App\Models\Picture;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class DownloadController extends Controller
{

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index($id, $type)
    {
        // $check = Files::find($id);
        // if(file_exists(public_path('storage/'.$check->url)))
        // {
        //     return response()->download(storage_path('app/public/'.$check->url), $check->filename);
        // }

        // return response()->view('errors.download');
    }

    public function picture($id)
    {
        $check = Picture::find($id);
        if(file_exists(public_path('storage/'.$check->url)))
        {
            return response()->download(public_path('storage/'.$check->url), $check->filename);
        }

        return response()->view('errors.download');
    }

    public function multiple($id, $type)
    {
        if($type != "undefined")
        {
            $check = Files::where('target_id', $id)->where('target_type', $type)->get();
            $files = [];
            if($check->count() > 0)
            {
                $rename = [];
                foreach($check as $c)
                {
                    if(file_exists(public_path('storage/'.$c->url)))
                    {
                        $newname = '';
                        $splitname = explode("/",$c->url);
                        for ($i=0; $i < count($splitname) - 1 ; $i++) { 
                            $newname .= $splitname[$i].'/';
                        }
                        Storage::disk('public')->move($c->url, $newname.$c->filename);
                        $files[] = public_path('storage/'.$newname.$c->filename);
                        $rename[] = [
                            'old' => $c->url,
                            'new' => $newname.$c->filename,
                        ];
                    }
                }
                $now = Carbon::now()->format('Ymdhis');
                Zipper::make(public_path('storage/'.$type.$now.'.zip'))->add($files)->close();
                
                for ($i=0; $i < count($rename) ; $i++) { 
                    Storage::disk('public')->move($rename[$i]['new'], $rename[$i]['old']);
                }

                if(file_exists(public_path('storage/'.$type.$now.'.zip')))
                {
                    return response()->download(public_path('storage/'.$type.$now.'.zip'));
                }
                return response()->view('errors.download');
            }

            return response()->view('errors.download');
        }
    }

    public function delete($id)
    {
        $file = Files::find($id);
        $file->delete();

        return $this->render('partials.file-tab.exist-file.lampiran', ['record' => $file->target]);
    }

    // public function deleteDaftarHadir($id)
    // {
    //     $file = DaftarHadir::find($id);
    //     $file->delete();

    //     return $this->render('partials.file-tab.exist-file.daftar-hadir', ['record' => $file->target]);
    // }

    // public function deletePicture($id)
    // {
    //     $file = Picture::find($id);
    //     $file->delete();

    //     return $this->render('partials.file-tab.exist-file.foto-rapat', ['record' => $file->target]);
    // }

    // public function deleteBeritaAcara($id)
    // {
    //     $data = RapatBeritaAcara::find($id);
    //     $data->delete();
    //     $file = FIles::where('target_id',$data->id)->where('target_type','rapat_berita_acara');
    //     $file->delete();

    //     // return $this->render('partials.file-tab.exist-file.foto-rapat', ['record' => $data->target]);
    // }
}

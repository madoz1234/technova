<?php

namespace App\Http\Controllers\Task;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Task\TaskRequest;
use App\Models\Auths\User;
use App\Models\Task\Task;

use DB;

class TaskController extends Controller
{
    protected $routes = 'task.task';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'tgl',
                'name' => 'tgl',
                'label' => 'Tanggal',
                'sortable' => true,
                'className' => 'text-center',
                'width' => '80px',
            ],
            [
                'data' => 'pic',
                'name' => 'pic',
                'label' => 'Nama Karyawan',
                'sortable' => true,
                'className' => 'text-center',
                'width' => '170px',
            ],
            [
                'data' => 'tugas',
                'name' => 'tugas',
                'label' => 'Tugas',
                'sortable' => true,
                'width' => '170px',
            ],
            [
                'data' => 'keterangan',
                'name' => 'keterangan',
                'label' => 'Keterangan',
                'sortable' => true,
                'width' => '300px',
            ],
            [
                'data' => 'lat',
                'name' => 'lat',
                'label' => 'Latitude',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'lng',
                'name' => 'lng',
                'label' => 'Longitude',
                'sortable' => true,
                'className' => 'text-center',
                'width' => '100px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'sortable' => true,
                'className' => 'text-center',
                'width' => '170px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '70px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
    	$user = auth()->user();
    	if($user->hasRole('admin')){
	        $records = Task::select('*');
    	}else{
    		$records = Task::where('user_id', $user->id)->select('*');
    	}
        if($name = request()->name) {
            $records->whereHas('employe', function($u) use ($name){
            	$u->where('name', 'like', '%' . $name . '%');
            });
        }
		if($tugas = request()->tugas) {
            $records->where('tugas', 'like', '%' . $tugas . '%');
        }
        if($tgl = request()->tgl) {
            $records->where('tgl', 'like', '%' . $tgl . '%');
        }
        return DataTables::of($records)
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->editColumn('tanggal', function ($record) {
                   return DateToString($record->tgl);
               })
               ->editColumn('pic', function ($record) {
                   return $record->employe->name;
               })
               ->editColumn('tugas', function ($record) {
                   return $record->tugas;
               })
               ->addColumn('keterangan', function ($record) {
                   return $record->readMoreText('keterangan', 150);
               })
               ->addColumn('lat', function ($record) {
                   return $record->lat;
               })
               ->addColumn('lng', function ($record) {
                   return $record->lng;
               })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->addColumn('status', function ($record) {
                    if($record->status == 1){
	                    return '<span class="label label-success">Aktif</span>'; 
	                }else{
	                    return '<span class="label label-danger">Tidak Aktif</span>';
	                }
               })
               ->addColumn('action', function ($record) use ($user){
                    $buttons = '';
                    $buttons .= $this->makeButtons([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
	           			'tooltip' 	=> 'Detail',
	           			'class' 	=> 'details button',
	           			'id'   		=> $record->id,
	           		]);
                    $buttons .='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    if($user->hasRole(['admin'])){
	                    $buttons .= $this->makeButton([
	                        'type' => 'edit',
	                        'id'   => $record->id,
	                    ]);
	                    
	                    $buttons .= $this->makeButton([
	                        'type' => 'delete',
	                        'id'   => $record->id,
	                    ]);
                    }

                   return $buttons;
               })
               ->rawColumns(['action','status','alamat','jk'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.task.index');
    }

    public function create()
    {
        return $this->render('modules.task.create');
    }

    public function store(TaskRequest $request)
    {
    	DB::beginTransaction();
    	try {
	    	$task 				= new Task;
		    $task->tgl 			= $request->tgl;
		  	$task->tugas 		= $request->tugas;
		  	$task->keterangan 	= $request->keterangan;
		  	$task->lat 			= $request->lat;
		  	$task->lng 			= $request->lng;
		  	$task->user_id 		= $request->user_id;
		  	if($request['status']){
	    		$task->status 		= 1;
	    	}else{
	    		$task->status 		= 0;
	    	}
		    $task->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function detail(Task $id)
    {
       return $this->render('modules.task.detail', ['record' => $id]);
    }

    public function edit(Task $task)
    {
        return $this->render('modules.task.edit', ['record' => $task]);
    }

    public function update(TaskRequest $request, Task $task)
    {
    	DB::beginTransaction();
    	try {
	    	$task 				= Task::find($request->id);
		    $task->tgl 			= $request->tgl;
		  	$task->tugas 		= $request->tugas;
		  	$task->keterangan 	= $request->keterangan;
		  	$task->lat 			= $request->lat;
		  	$task->lng 			= $request->lng;
		  	$task->user_id 		= $request->user_id;
		  	if($request['status']){
	    		$task->status 		= 1;
	    	}else{
	    		$task->status 		= 0;
	    	}
		    $task->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Task $task)
    {
        if($task){
    		$task->delete();
        	return response([
            	'status' => true,
	        ],200);
    	}else{
    		return response([
                'status' => 500,
            ],500);
    	}
    }
}

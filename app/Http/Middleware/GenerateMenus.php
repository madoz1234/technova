<?php
namespace App\Http\Middleware;

use Closure;
use Menu;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('sideMenu', function ($menu) {
            $menu->add('Dashboard', 'dashboard')
            	 ->data('perms', 'dashboard')
                 ->data('icon', 'fa fa-tachometer')
                 ->active('dashboard/*');
	        $menu->add('Task', 'task/task')
            	 ->data('perms', 'task/task')
                 ->data('icon', 'fa fa-calendar')
                 ->active('task/task/*');
	        $menu->add('Employees', 'setting')
                 ->data('perms', 'setting')
                 ->data('icon', 'fa fa-users')
                 ->active('setting/*');
        });
        return $next($request);
    }
}

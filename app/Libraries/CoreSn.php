<?php
namespace App\Libraries;

use Carbon\Carbon;

class CoreSn
{
    public static function DateToSql($date) {
        if($date != NULL)
        {
            $pecah = explode("-", $date);
            $tglStr = str_replace(",", "", $pecah[0]);
            if(strlen($tglStr) == 1)
            {
                $tglStr = "0".$tglStr;
            }
            $thnStr = $pecah[2];
            $blnStr = $pecah[1];
            switch ($pecah[0])
            {
                case 'Januari': $blnStr = '01';
                break;
                case 'Februari': $blnStr = '02';
                break;
                case 'Maret': $blnStr = '03';
                break;
                case 'April': $blnStr = '04';
                break;
                case 'Mei': $blnStr = '05';
                break;
                case 'Juni': $blnStr = '06';
                break;
                case 'Juli': $blnStr = '07';
                break;
                case 'Agustus': $blnStr = '08';
                break;
                case 'September': $blnStr = '09';
                break;
                case 'Oktober': $blnStr = '10';
                break;
                case 'November': $blnStr = '11';
                break;
                case 'Desember': $blnStr = '12';
                break;
            }
            return date($tglStr."-".$blnStr."-".$thnStr);
        }else{
            return NULL;
        }
    }
    public static function DateToString($date) {
        if(!$date)
        {
            return '-';
        }
        $date = \Carbon\Carbon::parse($date)->format('d-m-Y');
        $pecah = explode("-", $date);
        $thnStr = $pecah[0];
        $tglStr = $pecah[2].",";
        $blnStr = "";
        switch ($pecah[1])
        {
            case '01': $blnStr = 'Januari';
            break;
            case '02': $blnStr = 'Februari';
            break;
            case '03': $blnStr = 'Maret';
            break;
            case '04': $blnStr = 'April';
            break;
            case '05': $blnStr = 'Mei';
            break;
            case '06': $blnStr = 'Juni';
            break;
            case '07': $blnStr = 'Juli';
            break;
            case '08': $blnStr = 'Agustus';
            break;
            case '09': $blnStr = 'September';
            break;
            case '10': $blnStr = 'Oktober';
            break;
            case '11': $blnStr = 'November';
            break;
            case '12': $blnStr = 'Desember';
            break;
        }
        return $tglStr." ".$blnStr." ".$thnStr;
    }
    
    public static function DateToStringWDay($date) {
        if(!$date)
        {
            return '-';
        }
        $date = \Carbon\Carbon::parse($date)->format('d-m-Y');
        $pecah = explode("-", $date);
        $thnStr = $pecah[0];
        $tglStr = $pecah[2];
        $blnStr = "";
        $txtday = \Carbon\Carbon::parse($date)->format('l');
        switch ($txtday)
        {
            case 'Monday': $txtday = 'Senin';
            break;
            case 'Tuesday': $txtday = 'Selasa';
            break;
            case 'Wednesday': $txtday = 'Rabu';
            break;
            case 'Thursday': $txtday = 'Kamis';
            break;
            case 'Friday': $txtday = 'Jumat';
            break;
            case 'Saturday': $txtday = 'Sabtu';
            break;
            case 'Sunday': $txtday = 'Minggu';
            break;
        }
        switch ($pecah[1])
        {
            case '01': $blnStr = 'Januari';
            break;
            case '02': $blnStr = 'Februari';
            break;
            case '03': $blnStr = 'Maret';
            break;
            case '04': $blnStr = 'April';
            break;
            case '05': $blnStr = 'Mei';
            break;
            case '06': $blnStr = 'Juni';
            break;
            case '07': $blnStr = 'Juli';
            break;
            case '08': $blnStr = 'Agustus';
            break;
            case '09': $blnStr = 'September';
            break;
            case '10': $blnStr = 'Oktober';
            break;
            case '11': $blnStr = 'November';
            break;
            case '12': $blnStr = 'Desember';
            break;
        }
        return $txtday.', '.$tglStr." ".$blnStr." ".$thnStr;
    }

    public static function DateToStringWDays($date) {
        if(!$date)
        {
            return '-';
        }
        $date = \Carbon\Carbon::parse($date)->format('d-m-Y');
        $pecah = explode("-", $date);
        $thnStr = $pecah[2];
        $tglStr = $pecah[0];
        $blnStr = "";

        $txtday = \Carbon\Carbon::parse($date)->format('l');
        switch ($txtday)
        {
            case 'Monday': $txtday = 'Mon';
            break;
            case 'Tuesday': $txtday = 'Tue';
            break;
            case 'Wednesday': $txtday = 'Wed';
            break;
            case 'Thursday': $txtday = 'Thu';
            break;
            case 'Friday': $txtday = 'Fri';
            break;
            case 'Saturday': $txtday = 'Sat';
            break;
            case 'Sunday': $txtday = 'Sun';
            break;
        }
        switch ($pecah[1])
        {
            case '01': $blnStr = 'Jan';
            break;
            case '02': $blnStr = 'Feb';
            break;
            case '03': $blnStr = 'Mar';
            break;
            case '04': $blnStr = 'Apr';
            break;
            case '05': $blnStr = 'May';
            break;
            case '06': $blnStr = 'Jun';
            break;
            case '07': $blnStr = 'Jul';
            break;
            case '08': $blnStr = 'Aug';
            break;
            case '09': $blnStr = 'Sep';
            break;
            case '10': $blnStr = 'Oct';
            break;
            case '11': $blnStr = 'Nov';
            break;
            case '12': $blnStr = 'Dec';
            break;
        }
        return $txtday.' '.$blnStr." ".$tglStr." ".$thnStr;
    }

    public static function Day($date) {
        if(!$date)
        {
            return '-';
        }
        $date = \Carbon\Carbon::parse($date)->format('d-m-Y');
        $pecah = explode("-", $date);
        $thnStr = $pecah[0];
        $tglStr = $pecah[2];
        $blnStr = "";
        $txtday = \Carbon\Carbon::parse($date)->format('l');
        switch ($txtday)
        {
            case 'Monday': $txtday = 'Senin';
            break;
            case 'Tuesday': $txtday = 'Selasa';
            break;
            case 'Wednesday': $txtday = 'Rabu';
            break;
            case 'Thursday': $txtday = 'Kamis';
            break;
            case 'Friday': $txtday = 'Jum`at';
            break;
            case 'Saturday': $txtday = 'Sabtu';
            break;
            case 'Sunday': $txtday = 'Minggu';
            break;
        }
        switch ($pecah[1])
        {
            case '01': $blnStr = 'Januari';
            break;
            case '02': $blnStr = 'Februari';
            break;
            case '03': $blnStr = 'Maret';
            break;
            case '04': $blnStr = 'April';
            break;
            case '05': $blnStr = 'Mei';
            break;
            case '06': $blnStr = 'Juni';
            break;
            case '07': $blnStr = 'Juli';
            break;
            case '08': $blnStr = 'Agustus';
            break;
            case '09': $blnStr = 'September';
            break;
            case '10': $blnStr = 'Oktober';
            break;
            case '11': $blnStr = 'November';
            break;
            case '12': $blnStr = 'Desember';
            break;
        }
        return $txtday;
    }

    public static function Days($date) {
        if(!$date)
        {
            return '-';
        }
        $date = \Carbon\Carbon::parse($date)->format('d-m-Y');
        $pecah = explode("-", $date);
        $thnStr = $pecah[0];
        $tglStr = $pecah[2];
        $blnStr = "";
        $txtday = \Carbon\Carbon::parse($date)->format('d');
        return $txtday;
    }

    public static function Bulan($date) {
        if(!$date)
        {
            return '-';
        }
        $date = \Carbon\Carbon::parse($date)->format('d-m-Y');
        $pecah = explode("-", $date);
        $thnStr = $pecah[0];
        $tglStr = $pecah[2];
        $blnStr = "";
        $txtday = \Carbon\Carbon::parse($date)->format('l');
        switch ($txtday)
        {
            case 'Monday': $txtday = 'Senin';
            break;
            case 'Tuesday': $txtday = 'Selasa';
            break;
            case 'Wednesday': $txtday = 'Rabu';
            break;
            case 'Thursday': $txtday = 'Kamis';
            break;
            case 'Friday': $txtday = 'Jum`at';
            break;
            case 'Saturday': $txtday = 'Sabtu';
            break;
            case 'Sunday': $txtday = 'Minggu';
            break;
        }
        switch ($pecah[1])
        {
            case '01': $blnStr = 'Januari';
            break;
            case '02': $blnStr = 'Februari';
            break;
            case '03': $blnStr = 'Maret';
            break;
            case '04': $blnStr = 'April';
            break;
            case '05': $blnStr = 'Mei';
            break;
            case '06': $blnStr = 'Juni';
            break;
            case '07': $blnStr = 'Juli';
            break;
            case '08': $blnStr = 'Agustus';
            break;
            case '09': $blnStr = 'September';
            break;
            case '10': $blnStr = 'Oktober';
            break;
            case '11': $blnStr = 'November';
            break;
            case '12': $blnStr = 'Desember';
            break;
        }
        return $blnStr;
    }
}

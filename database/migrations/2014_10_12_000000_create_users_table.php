<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nip');
            $table->string('name');
            $table->integer('status')->default(1)->comment('0:Nonaktif,1:Aktif');
            $table->string('tgl_lahir', 255);
            $table->string('email')->unique();
            $table->integer('jk')->default(0)->comment('0:Laki-Laki,1:Perempuan');
            $table->string('password');
            $table->string('otp_token')->nullable();
            $table->text('alamat')->nullable();
            $table->dateTime('last_activity')->nullable()->comment('check idle');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_users');
    }
}

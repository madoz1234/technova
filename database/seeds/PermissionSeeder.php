<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// Reset cached roles and permissions
		app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
		
    	// create permissions
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('sys_role_has_permissions')->truncate();
        DB::table('sys_role_has_permissions')->truncate();
        DB::table('sys_model_has_roles')->truncate();
        DB::table('sys_permissions')->truncate();
        DB::table('sys_roles')->truncate();
        DB::table('sys_users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    	
    	$permissions = [
    		// ------------- DASHBOARD ---------------
    		[
				'name'         => 'dashboard',
				// 'guard_name' => 'Dashboard',
				'action'       => ['view'],
    		],
    	];
    	foreach ($permissions as $row) {
    		foreach ($row['action'] as $key => $val) {
    			$temp = [
					'name'         => $row['name'].'-'.$val,
    			];	
				Permission::create($temp);
    		}
		}
		
		$role1 = Role::create(['name' => 'admin']);
		$role2 = Role::create(['name' => 'employe']);
        $role1->givePermissionTo(Permission::all());
        $role2->givePermissionTo(Permission::all());
    }
}

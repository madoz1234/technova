@push('js')
	<script src="{{ asset('libs/assets/swal/sweetalert.min.js') }}"></script>
	<script src="{{ asset('libs/jquery/form/jquery.form.min.js') }}"></script>
@endpush

@push('scripts')
	<script type="text/javascript">
		function saveData(formid, callback) 
		{
			// show loading
			$('#' + formid).find('.loading.dimmer').show();
			// begin submit
			$("#" + formid).ajaxSubmit({
				success: function(resp){
					swal({
						icon: "success",
						title:'Berhasil!',
						text:'Data berhasil disimpan.',
						button: false,
						timer: 1000,
					}).then((result) => { 
						$('#' + formid).find('.loading.dimmer').hide();
						callback();
						dt1 = $('#dataTable1').DataTable();
						dt1.draw();
						dt2 = $('#dataTable2').DataTable();
						dt2.draw();
						dt3 = $('#dataTable3').DataTable();
						dt3.draw();
					})
				},
				error: function(resp){
					$('#' + formid).find('.loading.dimmer').hide();
					// $('#cover').hide();
					var response = resp.responseJSON;

					$.each(response.errors, function(index, val) {
						var name = index.split('.')
										.reduce((all, item) => {
											all += (index == 0 ? item : '[' + item + ']');
											return all;
										});
						var fg = $('[name="'+ name +'"], [name="'+ name +'[]"]').closest('.form-group');
						fg.addClass('has-error');
						fg.append('<small class="control-label error-label font-bold">'+ val +'</small>')
					});

					var intrv = setInterval(function(){
						$('.form-group .error-label').slideUp(500, function(e) {
							$(this).remove();
							$('.form-group.has-error').removeClass('has-error');
							clearTimeout(intrv);
						});
					}, 3000)
				}
			});
		}
		
		function readMoreItem(element){
			$("."+element).each(function() {
				var display = parseInt($(this).data('display'));
				var list = 0;
				$(this).find('.item').each(function(){
					if(list>=display){
						$(this).addClass('hidden-item')
						$(this).prop("style", "display:none")
					}
					list++;
				});
				if(list-1 >= display){
					var html = `<a class="button-more" style="cursor:pointer;color:blue;">Selengkapnya.....</a>`;
					$(this).append(html);
				}
			});

			$(document).on('click', '.'+element+' .button-more', function (e) {
				$(this).closest('.'+element).find('.hidden-item').each(function(){
					$(this).prop('style','');
				});
				$(this).removeClass('button-more');
				$(this).addClass('button-less');
				$(this).text('Kecilkan.....');
			});

			$(document).on('click', '.'+element+' .button-less', function (e) {
				$(this).closest('.'+element).find('.hidden-item').each(function(){
					$(this).prop("style", "display:none")
				});
				$(this).removeClass('button-less');
				$(this).addClass('button-more');
				$(this).text('Selengkapnya.....');
			});
		}

		function deleteData(url, callback) 
		{
			swal({
				title: "Apakah Anda yakin?",
				text: "Data yang sudah dihapus, tidak dapat dikembalikan!",
				icon: "warning",
				buttons: {
					cancel: {
						text: "Batal",
						value: null,
						visible: true,
						className: "swal-button--danger",
						closeModal: true,
					},
					confirm: {
						text: "OK",
						value: true,
						visible: true,
						className: "swal-button--cancel",
						closeModal: true
					},
				},
			}).then((result) => {
				if (result) {
					$.ajax({
						url: url,
						type: 'POST',
						data: {
							'_method' : 'DELETE',
							'_token' : '{{ csrf_token() }}'
						}
					})
					.done(function(response) {
						swal({
							icon: 'success',
							title:'Terhapus!',
							text:'Data berhasil dihapus!',
							button: false,
							timer: 1000,
					    }).then((res) => {
					    	callback()
					    })
					})
					.fail(function(response) {
						console.log(response);
						swal('Data gagal dihapus!', {
							icon: 'error',
					    }).then((res) => {
					    	callback()
					    })
					})

				}
			})
		}

		function loadModal(param, callback) 
		{
			var url    = (typeof param['url'] === 'undefined') ? '#' : param['url'];
			var modal  = (typeof param['modal'] === 'undefined') ? 'mediumModal' : param['modal'];
			var formId = (typeof param['formId'] === 'undefined') ? 'formData' : param['formId'];
			var onShow = (typeof callback === 'undefined') ? function(){} : callback;
			var modals = $(modal);

			modals.find('.modal-content').html(`
                <div class="loading dimmer" style="height: 200px; top: calc(50% - 100px)">
                    <div class="loader"></div>
                </div>
			`);

			modals.modal('show');
			
			modals.off('shown.bs.modal');
			modals.on('shown.bs.modal', function(event) {
				modals.find('.modal-content').load(url, callback);
			});
		}

		function loadModals(param, callback) 
		{
			var url    = (typeof param['url'] === 'undefined') ? '#' : param['url'];
			var modal  = (typeof param['modal'] === 'undefined') ? 'largeModal' : param['modal'];
			var formId = (typeof param['formId'] === 'undefined') ? 'formData' : param['formId'];
			var onShow = (typeof callback === 'undefined') ? function(){} : callback;
			var modals = $(modal);

			modals.find('.modal-content').html(`
                <div class="loading dimmer" style="height: 200px; top: calc(50% - 100px)">
                    <div class="loader"></div>
                </div>
			`);

			modals.modal('show');
			
			modals.off('shown.bs.modal');
			modals.on('shown.bs.modal', function(event) {
				modals.find('.modal-content').load(url, callback);
			});
		}

		function loadModalPreview(param, callback) 
		{
			var url    = (typeof param['url'] === 'undefined') ? '#' : param['url'];
			var modal  = '#largeModal';
			var formId = (typeof param['formId'] === 'undefined') ? 'formData' : param['formId'];
			var onShow = (typeof callback === 'undefined') ? function(){} : callback;
			var modals = $(modal);
			modals.find('.modal-content').html(`
                <div class="loading dimmer" style="height: auto; top: calc(50% - 100px)">
                    <div class="loader"></div>
                </div>
			`);

			modals.modal('show');
			
			modals.off('shown.bs.modal');
			modals.on('shown.bs.modal', function(event) {
				modals.find('.modal-content').load(url, callback);
			});
		}

		function postNewTab(url, param)
		{
	        var form = document.createElement("form");
	        form.setAttribute("method", 'POST');
	        form.setAttribute("action", url);
	        form.setAttribute("target", "_blank");

	        $.each(param, function(key, val) {
	            var inputan = document.createElement("input");
	                inputan.setAttribute("type", "hidden");
	                inputan.setAttribute("name", key);
	                inputan.setAttribute("value", val);
	            form.appendChild(inputan);
	        });

	        document.body.appendChild(form);
	        form.submit();

	        document.body.removeChild(form);
	    }

	    function getNewTab(url)
	    {
	        var win = window.open(url, '_blank');
	  		win.focus();
	    }

		showFormError = function(key, value)
		{
			if(key.includes("."))
			{
				res = key.split('.');
				key = res[0] + '[' + res[1] + ']';
				if(res[1] == 0)
				{
					key = res[0] + '\\[\\]';
					var exist = $('#dataForm' + ' [name="' + res[0] + '[' + res[1] + ']' + '"]');
					if(exist.length > 0)
					{
						key = res[0] + '[' + res[1] + ']';
					}
				}
				if(res[2])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']';
					if(res[2] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '\\[\\]';
					}
				}
				if(res[3])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']';
					if(res[3] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '\\[\\]';
					}
				}
				if(res[4])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']';
					if(res[4] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '\\[\\]';
					}
				}
				if(res[5])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']' + '[' + res[5] + ']';
					if(res[5] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '['+ res[4] +']' + '\\[\\]';
					}
				}
				if(res[6])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']' + '[' + res[5] + ']' + '[' + res[6] + ']';
					if(res[6] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '['+ res[4] +']' + '['+ res[5] +']' + '\\[\\]';
					}
				}
			}
			var elm = $('[name^="'+ key +'"]').closest('.field');
			var tabs = $('[name^="'+ key +'"]').parents('.tab.segment');
			if(tabs.length > 0)
			{
				selectedTabs(tabs);
			}

			// var fg = $('[name="'+ name +'"], [name="'+ name +'[]"]').closest('.form-group');
			// fg.addClass('has-error');
			// fg.append('<small class="control-label error-label font-bold">'+ val +'</small>')

			var message = `<small class="control-label error-label font-bold">`+ value +`</small>`;
			var showerror = $('[name^="'+ key +'"]').closest('.field');
			var multipleCheckbox = $(showerror).parents('.multiple-checkbox');
			if($('[name^="'+ key +'"]').closest('.field').find('input').length > 0 && $('[name^="'+ key +'"]').closest('.field').find('input').hasClass('hidden'))
			{
				$(elm).addClass('has-error');
				if($('[name^="'+ key +'"]').closest('.field').find('div').length > 0)
				{
					$('[name^="'+ key +'"]').closest('.field').find('div').append('<small class="control-label error-label font-bold">' + value + '</small>');
				}else{
					$(showerror).append('<small class="control-label error-label font-bold">' + value + '</small>');
				}
			}else{
				if(multipleCheckbox.length > 0)
				{
					multipleCheckboxLabel = multipleCheckbox.find('label:first-child');
					$(multipleCheckboxLabel).append('<span class="red error-label" style="color:#9f3a38 !important;">' + value + '</span>');
				}else{
					$(elm).addClass('has-error');
					$(showerror).append('<small class="control-label error-label font-bold">' + value + '</small>');
				}
			}
		}

		clearFormError = function(key, value)
		{
			if(key.includes("."))
			{
				res = key.split('.');
				key = res[0] + '[' + res[1] + ']';
				if(res[1] == 0)
				{
					key = res[0] + '\\[\\]';
					var exist = $(' [name="' + res[0] + '[' + res[1] + ']' + '"]');
					if(exist.length > 0)
					{
						key = res[0] + '[' + res[1] + ']';
					}
				}
				if(res[2])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']';
					if(res[2] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '\\[\\]';
					}
				}
				if(res[3])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']';
					if(res[3] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '\\[\\]';
					}
				}
				if(res[4])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']';
					if(res[4] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '\\[\\]';
					}
				}
				if(res[5])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']' + '[' + res[5] + ']';
					if(res[5] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '['+ res[4] +']' + '\\[\\]';
					}
				}

				if(res[6])
				{
					key = res[0] + '[' + res[1] + ']' + '[' + res[2] + ']' + '[' + res[3] + ']' + '[' + res[4] + ']' + '[' + res[5] + ']' + '[' + res[6] + ']';
					if(res[6] == 0)
					{
						key = res[0] + '['+ res[1] +']' + '['+ res[2] +']' + '['+ res[3] +']' + '['+ res[4] +']' + '['+ res[5] +']' + '\\[\\]';
					}
				}
			}

			if($('[name^="'+ key +'"]').closest('.field').find('div').length > 0){
				var elm = $('[name^="'+ key +'"]').closest('.field');
				$(elm).removeClass('has-error');

				var showerror = $('[name^="'+ key +'"]').closest('.field').find('.control-label.error-label.font-bold').remove();
			}else{
				var elm = $('[name^="'+ key +'"]').closest('.field');
				$(elm).removeClass('has-error');

				var showerror = $('[name^="'+ key +'"]').closest('.field').find('.control-label.error-label.font-bold').remove();
			}
		}

		function showLoadingInput(elemchild)
		{
			var loading = `<div class="ui active mini centered inline loader"></div>`;

			$('#'+elemchild).parent().closest('.field').addClass('disabled');
			$('#'+elemchild).parent().closest('.field').append(loading);
		}

		function  stopLoadingInput(elemchild)
		{
			$('#'+elemchild).parent().closest('.field').removeClass('disabled');
			$('#'+elemchild).parent().closest('.field').find('.inline.loader').remove();
		}


        function readUrl(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $(input).parent().find('img');

                reader.onload = function(e) {
                    img.attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).on('change', '.fileimg', function() {
            readUrl(this);
        });

        $(document).on('click', '.browse.button', function() {
            $(this).closest('.img-preview')
            	   .find('.fileimg')
            	   .trigger('click');
        });

		// We can attach the `fileselect` event to all file inputs on the page
		$(document).on('change', ':file', function() {
			var input = $(this),
			numFiles = input.get(0).files ? input.get(0).files.length : 1,
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [numFiles, label]);
		});

		// We can watch for our custom `fileselect` event like this
		$(document).on('fileselect', ':file', function(event, numFiles, label) {
			var input = $(this).parents('.input-group').find(':text'),
			log = numFiles > 1 ? numFiles + ' files selected' : label;

			if( input.length ) {
				input.val(log);
			} else {
				if( log ) console.log(log);
			}
		});

		$(document).on('click', '.save.as.page', function(e){
			$('#formData').find('input[name="status"]').val("1");
			var formDom = "formData";
			if($(this).data("form") !== undefined){
				formDom = $(this).data('form');
			}
			swal({
					title: "Apakah Anda yakin?",
					text: "Data yang sudah dikirim, tidak dapat diubah!",
					icon: "warning",
					buttons: ['Batal', 'OK'],
					reverseButtons: true
			}).then((result) => {
				if (result) {
					saveForm(formDom);
				}
			})
		});

		$(document).on('click', '.save.as.page2', function(e){
			var formDom = "formData";
			if($(this).data("form") !== undefined){
				formDom = $(this).data('form');
			}
			swal({
					title: "Apakah Anda yakin?",
					text: "Data yang sudah dikirim, tidak dapat diubah!",
					icon: "warning",
					buttons: ['Batal', 'OK'],
					reverseButtons: true
			}).then((result) => {
				if (result) {
					saveForm(formDom);
				}
			})
		});

		//Save As Draft
        $(document).on('click', '.save-draft.page', function(e){
        	$('#formData').find('input[name="status"]').val("0");
			var formDom = "formData";
			if($(this).data("form") !== undefined){
				formDom = $(this).data('form');
			}
			swal({
					title: "Apakah Anda yakin?",
					text: "Data yang sudah dikirim, tidak dapat diubah!",
					icon: "warning",
					buttons: ['Batal', 'OK'],
					reverseButtons: true
			}).then((result) => {
				if (result) {
					saveForm(formDom);
				}
			})
     //        $('[name=status]').val(0);
     //        swal({
     //            title: "Apakah Anda yakin?",
					// text: "Data yang sudah dikirim, tidak dapat diubah!",
					// icon: "warning",
					// buttons: true,
					// reverseButtons: true
     //        }).then((result) => {
     //            if (result) {
     //                saveData('formData', 'form');
     //            }
     //        })
        });

        $(document).on('click', '.save.as.page.flag', function(e){
			$('#formData').find('input[name="flag"]').val("1");
			var formDom = "formData";
			if($(this).data("form") !== undefined){
				formDom = $(this).data('form');
			}
			swal({
					title: "Apakah Anda yakin?",
					text: "Data yang sudah dikirim, tidak dapat diubah!",
					icon: "warning",
					buttons: ['Batal', 'OK'],
					reverseButtons: true
			}).then((result) => {
				if (result) {
					saveForm(formDom);
				}
			})
		});

        $(document).on('click', '.save-draft.page.flag', function(e){
        	$('#formData').find('input[name="flag"]').val("0");
			var formDom = "formData";
			if($(this).data("form") !== undefined){
				formDom = $(this).data('form');
			}
			swal({
					title: "Apakah Anda yakin?",
					text: "Data yang sudah dikirim, tidak dapat diubah!",
					icon: "warning",
					buttons: ['Batal', 'OK'],
					reverseButtons: true
			}).then((result) => {
				if (result) {
					saveForm(formDom);
				}
			})
        });

        // status rapat
        $(document).on('click', '.save.as.page.status-rapat', function(e){
			$('#formData').find('input[name="status_rapat"]').val("1");
			var formDom = "formData";
			if($(this).data("form") !== undefined){
				formDom = $(this).data('form');
			}
			swal({
					title: "Apakah Anda yakin?",
					text: "Data yang sudah dikirim, tidak dapat diubah!",
					icon: "warning",
					buttons: ['Batal', 'OK'],
					reverseButtons: true
			}).then((result) => {
				if (result) {
					saveForm(formDom);
				}
			})
		});

        $(document).on('click', '.save-draft.page.status-rapat', function(e){
        	$('#formData').find('input[name="status_rapat"]').val("0");
			var formDom = "formData";
			if($(this).data("form") !== undefined){
				formDom = $(this).data('form');
			}
			swal({
					title: "Apakah Anda yakin?",
					text: "Data yang sudah dikirim, tidak dapat diubah!",
					icon: "warning",
					buttons: ['Batal', 'OK'],
					reverseButtons: true
			}).then((result) => {
				if (result) {
					saveForm(formDom);
				}
			})
        });

		$(document).on('click', '.save.as.drafting', function(e){
			$('#formData').find('input[name="status"]').val("0");
			saveForm();
		});

		function saveForm(form="formData")
			{
				$('#' + form).find('.loading.dimmer').show();
				$("#"+form).ajaxSubmit({
					success: function(resp){
						$("#formModal").modal('hide');
						swal(
							{
								type: 'success',
								title: 'Tersimpan!',
								text: 'Data berhasil disimpan.',
								icon: "success",
								buttons: false,
								timer: 2000
							}
							).then((result) => {
								$('#' + form).find('.loading.dimmer').hide();
								var url = "{!! route($routes.'.index') !!}";
					            window.location = url;
								return true;
							})
						},
					error: function(resp){
						$('#' + form).find('.loading.dimmer').hide();
						if (resp.responseJSON.status !== "undefined" && resp.responseJSON.status === 'false'){
							swal(
								'Oops, Maaf!',
								resp.responseJSON.message,
								'error'
								)
						}

						$('#cover').hide();
						var response = resp.responseJSON;
						$.each(response.errors, function(index, val) {
							clearFormError(index,val);
							showFormError(index,val);
						});
						time = 5;
						interval = setInterval(function(){
							time--;
							if(time == 0){
								clearInterval(interval);
								$('.pointing.prompt.label.transition.visible').remove();
								$('.field .error-label').slideUp(500, function(e) {
									$(this).remove();
									$('.field.has-error').removeClass('has-error');
									clearTimeout(interval);
								});
							}
						},1000)
					}
				});
			}
	</script>
@endpush
<form action="" id="formData">
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Detil Task</h5>
    </div>
    <div class="modal-body">
    	<table class="table table-bordered" style="font-size: 12px;">
			<tbody>
				<tr>
					<td style="width: 110px;">Tanggal</td>
					<td style="width: 10px;">:</td>
					<td style="text-align: left;">
						{{ $record->tgl }}
					</td>
				</tr>
				<tr>
					<td>Employe</td>
					<td>:</td>
					<td style="text-align: left;">
						{{ $record->employe->name }}
					</td>
				</tr>
				<tr>
					<td>Tugas</td>
					<td>:</td>
					<td style="text-align: left;">
						{{$record->tugas}}
					</td>
				</tr>
				<tr>
					<td>Latitude</td>
					<td>:</td>
					<td style="text-align: left;">
						{{$record->lat}}
					</td>
				</tr>
				<tr>
					<td>Longitude</td>
					<td>:</td>
					<td style="text-align: left;">
						{{$record->lng}}
					</td>
				</tr>
				<tr>
					<td>Keterangan</td>
					<td>:</td>
					<td style="text-align: left;text-justify: inter-word;">
						{!! $record->readMoreText('keterangan', 150) !!}
					</td>
				</tr>
				<tr>
					<td>Status</td>
					<td>:</td>
					<td style="text-align: left;">
						@if($record->status == 1)
							<span class="label label-success">Aktif</span>
						@else 
							<span class="label label-danger">Tidak Aktif</span>
						@endif
					</td>
				</tr>
			</tbody>
		</table>
		<div class="row row-sm">
			<div class="col-lg-12 col-md-3 col-sm-3">
				<div class="row" style="margin: 0px 0px 20px 0px">
						<div class="col-sm-12 text-right">
							<div id="map" style="height:300px"></div>
						</div>
				</div>
			</div>
		</div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
    </div>
    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>
<script>
	 var lat = <?php echo json_encode($record->lat); ?>;
	 var lng = <?php echo json_encode($record->lng); ?>;
	 var name = <?php echo json_encode($record->employe->name); ?>;
    function initMap() {
        var new_marker;
        var markers = [];
        var icon_center ={
          url: '{{ asset('src/img/usr.png') }}',
          scaledSize: new google.maps.Size(30, 30),
        };
        // Create the map.
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: {lat: -6.1742526, lng: 106.7862555},
          // mapTypeId: 'Map'
        });
        var post;
        var post = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
        var new_marker = new google.maps.Marker({
            position: post,
            map: map,
            icon:icon_center,
            title: name,
        });
        markers.push(new_marker);
        
        var circle = new google.maps.Circle({
          map: map,
          radius: 10000,    // 10 miles in metres
          fillColor: "#4e4efc",//'#fc4e59',
          strokeColor: "#4e4efc",
          strokeOpacity: 0.8,
          strokeWeight: 0,
          fillOpacity: 0.4,
        });
        circle.bindTo('center', new_marker, 'position');
    };
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFlm1JrVjXexh-QSlQJYVcP_4dKSNVCJU&callback=initMap">
</script>

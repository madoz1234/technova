<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
    @method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Ubah Data Task</h5>
    </div>
    <div class="modal-body">
        <div class="clearfix">
            <div class="col-sm-6">
                <div class="form-group" style="margin-right: 13px;">
                    <label class="control-label">Tanggal</label>
            		<input type="text" name="tgl" class="form-control tgl" placeholder="Tanggal" required="" value="{{ $record->tgl }}">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">Tugas</label>
            		<input type="text" name="tugas" class="form-control" placeholder="Tugas" required="" value="{{ $record->tugas }}">
                </div>
            </div>
        </div>
        <div class="clearfix">
            <div class="col-sm-6">
                <div class="form-group" style="margin-right: 13px;">
                    <label class="control-label">Latitude</label>
                    <input type="text" name="lat" class="form-control" placeholder="Latitude" required="" value="{{ $record->lat }}">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">Longitude</label>
                    <input type="text" name="lng" class="form-control" placeholder="Longitude" required="" value="{{ $record->lng }}">
                </div>
            </div>
        </div>
        <div class="form-group">
	        <label class="control-label">Employe</label>
            <select class="selectpicker form-control show-tick" data-size="3" name="user_id" data-style="btn-default" data-live-search="true" title="(Pilih Employe)">
                @foreach(App\Models\Auths\User::whereHas('roles', function($u) use ($record){
                			$u->where('name', 'employe');
                		})->get() as $user)
                    <option value="{{ $user->id }}" @if($user->id == $record->user_id) selected @endif>{{ $user->name }}</option>
                @endforeach
            </select>               
        </div>
        <div class="form-group">
	        <label class="control-label">Keterangan</label>
            <textarea class="form-control" name="keterangan" id="exampleFormControlTextarea1" placeholder="Keterangan" rows="2">{{ $record->keterangan }}</textarea>                 
        </div>
        <div class="form-group">
	        <label class="control-label">Status</label><br>
            <input type="checkbox" name="status" data-width="100" data-toggle="toggle" data-size="mini" @if($record->status == 1) checked @endif data-on="Aktif" data-off="Nonaktif" data-style="ios">            
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>
@extends('layouts.list')

@section('title', 'Task')
@section('filters')
    <div class="form-group">
        <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
        <label class="control-label sr-only" for="filter-tgl">Tanggal</label>
        <input type="text" class="form-control filter-control tanggal" name="filter[tgl]" data-post="tgl" placeholder="Tanggal">
    </div>
    <div class="form-group m-l-sm">
        <label class="control-label sr-only" for="filter-name">Nama</label>
        <input type="text" class="form-control filter-control" name="filter[name]" data-post="name" placeholder="Nama">
    </div>
    <div class="form-group m-l-sm">
        <label class="control-label sr-only" for="filter-tugas">Tugas</label>
        <input type="text" class="form-control filter-control" name="filter[tugas]" data-post="tugas" placeholder="Tugas">
    </div>
@endsection
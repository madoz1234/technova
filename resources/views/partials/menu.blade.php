@php 
	$user = auth()->user();
@endphp
@foreach($items as $item)
        @if($item->link->path['url'] == 'title')
            <li class="hidden-folded padder m-t m-b-sm text-xs">
                @if(!is_null($item->icon))
                    <i class="{{ $item->icon }} icon pull-right"></i>
                @endif
                <span style="font-weight: bold;">{!! $item->title !!}</span>
            </li>
        @elseif($item->link->path['url'] == 'separator')
            <li class="line thin dk"></li>
        @elseif(!$item->hasChildren())
            <li class="{{ $item->isActive ? 'active' : '' }}" style="margin-left: -22px;">
                <a href="{!! $item->url() !!}" tabindex="{{ $item->id }}" class="{{ $item->link->class }}">
                    @if($item->link->path['url'] == 'mail') @endif
                    <i class="{{ $item->icon }} icon" style="left: 10px;"></i>
                    <span style="font-weight: bold;">{!! $item->title !!}&nbsp;@if($item->nickname !== 'dashboard' && $item->nickname !== 'manajemenPengguna')
                    	@if(bubblehead($item->nickname, $user) > 0)<span class="badge badge-sm down bg-danger pull-right-xs">{{ bubblehead($item->nickname, $user)}}</span>
                    	@endif
                    @endif</span>
                </a>
                @if(array_key_exists('with-header', $item->data) && $item->data['with-header'])
                <ul class="nav nav-sub">
                    <li class="nav-sub-header">
                       <a href="{!! $itemsz->url() !!}">
                           <span style="font-weight: bold;">{!! $item->title !!}</span>
                       </a>
                    </li>
                </ul>
                @endif
            </li>
        @else
            <li class="{{ $item->isActive ? 'active' : '' }}" style="margin-left: -22px;">
                <a href="{!! $item->url() !!}" tabindex="{{ $item->id }}" class="{{ $item->link->class }}">
                    <span class="pull-right text-muted">
                        <i class="fa fa-fw fa-angle-right fa-sm text"></i>
                        <i class="fa fa-fw fa-angle-down text-active"></i>
                    </span>
                    <i class="{{ $item->icon }} icon" style="left: 10px;"></i>
                    <span style="font-weight: bold;">{!! $item->title !!}&nbsp;
                    	@if($item->nickname !== 'dataMaster')
                    		@if(bubblehead($item->nickname, $user) > 0)<span class="badge badge-sm down bg-danger pull-right-xs">{{ bubblehead($item->nickname, $user)}}</span>
                    		@endif
                    	@endif
                	</span>
                </a>
                <ul class="nav nav-sub" style="margin-left: 10px;">
                	@foreach($item->children() as $itemsz)
                		@if(!$itemsz->hasChildren())
                			<li class="{{ $itemsz->isActive ? 'active' : '' }}" style="margin-left: -20px;">
				                <a href="{!! $itemsz->url() !!}" style="margin-left: 15px;" tabindex="{{ $itemsz->id }}" class="{{ $itemsz->link->class }}">
				                    @if($itemsz->link->path['url'] == 'mail') @endif
				                    <span style="font-weight: bold;">{!! $itemsz->title !!}&nbsp;
				                    	@if($itemsz->nickname !== 'project' && $itemsz->nickname !== 'businessUnit(BU)' && $itemsz->nickname !== 'corporateOffice(CO)' && $itemsz->nickname !== 'anakPerusahaan' && $itemsz->nickname !== 'vendor' && $itemsz->nickname !== 'dokumenPendahuluan' && $itemsz->nickname !== 'fokusAudit' && $itemsz->nickname !== 'langkahKerja' && $itemsz->nickname !== 'kategoriTemuan' && $itemsz->nickname !== 'kriteriaTemuan' && $itemsz->nickname !== 'survei' && $itemsz->nickname !== 'dateline')
				                    		@if(bubblechild($itemsz->nickname, $user) > 0)<span class="badge badge-sm down bg-danger pull-right-xs">{{ bubblechild($itemsz->nickname, $user)}}</span>
				                    		@endif
				                    	@endif
				                   	</span>
				                </a>
				                @if(array_key_exists('with-header', $itemsz->data) && $itemsz->data['with-header'])
				                <ul class="nav nav-sub">
				                    <li class="nav-sub-header">
				                       <a href="{!! $itemsz->url() !!}">
				                           <span style="font-weight: bold;">{!! $itemsz->title !!}</span>
				                       </a>
				                    </li>
				                </ul>
				                @endif
				            </li>
                		@else 
	                		<li class="{{ $itemsz->isActive ? 'active' : '' }}" style="margin-left: -20px;">
	                			<a href="#">
	                				<span class="pull-right text-muted">
	                					<i class="fa fa-fw fa-angle-right fa-sm text"></i>
	                					<i class="fa fa-fw fa-angle-down text-active"></i>
	                				</span>
	                				<span style="font-weight: bold;margin-left: 15px;">{!! $itemsz->title !!}&nbsp;
	                					@if($itemsz->nickname !== 'sPIN' && $itemsz->nickname !== 'kuesionerIACM')
	                						@if(bubblechild($itemsz->nickname, $user) > 0)<span class="badge badge-sm down bg-danger pull-right-xs">{{ bubblechild($itemsz->nickname, $user)}}</span>
	                						@endif
	                					@endif
	                				</span>
	                			</a>
	                			<ul class="nav nav-subz" style="margin-left: 10px;">
	                				@foreach($itemsz->children() as $itemszz)
	                				@if(!$itemszz->hasChildren())
		                				<li class="{{ $itemszz->isActive ? 'active' : '' }}" style="margin-left: -1px;">
		                					<a href="{!! $itemszz->url() !!}" tabindex="{{ $itemszz->id }}" class="{{ $itemszz->link->class }}">
		                						@if($itemszz->link->path['url'] == 'mail') @endif
		                						<i class="{{ $itemszz->icon }} icon"></i>
		                						<span style="font-weight: bold;margin-left: 15px;">{!! $itemszz->title !!}&nbsp;
		                							@if($itemszz->nickname !== 'pointOfFocus(PoF)' && $itemszz->nickname !== 'unsurPemenuhan' && $itemszz->nickname !== 'pembobotan' && $itemszz->nickname !== 'kertasKerja' && $itemszz->nickname !== 'elemen' && $itemszz->nickname !== 'keyProcessArea' && $itemszz->nickname !== 'uraian')
		                								@if(bubblechilds($itemszz->nickname, $user, $itemszz->title) > 0)<span class="badge badge-sm down bg-danger pull-right-xs">{{ bubblechilds($itemszz->nickname, $user, $itemszz->title)}}</span>
		                								@endif
		                							@endif
		                						</span>
		                					</a>
		                				</li>
	                				@else 
	                				@endif
	                				@endforeach
	                			</ul>
	                		</li>
                		@endif
                	@endforeach
                </ul>
            </li>
        @endif
@endforeach



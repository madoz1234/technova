<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
    @csrf

    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Tambah Data User</h5>
    </div>
    <div class="modal-body">
        <div class="clearfix">
            <div class="col-sm-6">
                <div class="form-group" style="margin-right: 13px;">
                    <label class="control-label">NIP</label>
            		<input type="text" name="nip" class="form-control" placeholder="NIP" required="">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">Nama</label>
            		<input type="text" name="name" class="form-control" placeholder="Nama" required="">
                </div>
            </div>
        </div>
        <div class="clearfix">
            <div class="col-sm-6">
                <div class="form-group" style="margin-right: 13px;">
                    <label class="control-label">Tanggal Lahir</label>
                    <input type="text" name="tgl_lahir" class="form-control tgl" placeholder="Tanggal Lahir" required="">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">Email</label>
                    <input type="email" name="email" class="form-control" placeholder="Email" required="">
                </div>
            </div>
        </div>
        <div class="clearfix">
            <div class="col-sm-6">
                <div class="form-group" style="margin-right: 13px;">
                    <label class="control-label">Jenis Kelamin</label>
                    <select class="selectpicker form-control" name="jk" data-style="btn-default" data-live-search="true" title="(Pilih Jenis Kelamin)">
                    	<option value="0">Laki - Laki</option>
                    	<option value="1">Perempuan</option>
           			</select>    
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
			        <label class="control-label">Role</label>
		            <select class="selectpicker form-control" name="role" data-style="btn-default" data-live-search="true" title="(Pilih Role)">
		                @foreach(App\Models\Auths\Role::get() as $role)
		                    <option value="{{ $role->id }}">{{ $role->name }}</option>
		                @endforeach
		            </select>                  
                </div>
            </div>
        </div>
        <div class="form-group">
	        <label class="control-label">Alamat Lengkap</label>
            <textarea class="form-control" name="alamat" id="exampleFormControlTextarea1" placeholder="Alamat" rows="2"></textarea>                 
        </div>
        <div class="clearfix">
            <div class="col-sm-6">
                <div class="form-group" style="margin-right: 13px;">
                   <label class="control-label">Password <em class="text-muted">(Min. 6 Karakter)</em></label>
           		   <input type="password" name="password" class="form-control" placeholder="Password" required="">   
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
			        <label class="control-label">Status</label><br>
		            <input type="checkbox" name="status" data-width="100" data-toggle="toggle" data-size="mini" checked data-on="Aktif" data-off="Nonaktif" data-style="ios">            
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>
@push('scripts')
    <script>
    </script>
    @yield('js-extra')
@endpush
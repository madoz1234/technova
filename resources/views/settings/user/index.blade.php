@extends('layouts.list')

@section('title', 'Manajemen Pengguna')

@section('filters')
    <div class="form-group">
        <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
        <label class="control-label sr-only" for="filter-name">Nama</label>
        <input type="text" class="form-control filter-control" name="filter[name]" data-post="name" placeholder="Nama">
    </div>
    <div class="form-group m-l-sm">
        <label class="control-label sr-only" for="filter-email">E-Mail</label>
        <input type="text" class="form-control filter-control" name="filter[email]" data-post="email" placeholder="Email">
    </div>
    <label class="control-label sr-only" for="filter-role">Role</label>
    <select class="select selectpicker filter-control show-tick" name="filter[role]" data-style="btn-default" data-post="role" title="Role" data-size="5" data-live-search="true">
    	<option style="font-size: 12px;" value="">(Pilih Salah Satu)</option>
        @foreach(Spatie\Permission\Models\Role::get() as $role)
            <option value="{{ $role->id }}">{{ $role->name }}</option>
        @endforeach
    </select>
@endsection